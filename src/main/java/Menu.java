import java.util.Scanner;

public class Menu {

    private final Scanner sc = new Scanner(System.in);

    void menuTop(){
        System.out.println("\n=================================================");
        System.out.println("Aplikasi Pengolah Nilai Siswa");
        System.out.println("=================================================");
    }

    void menuAwal(){
        WriteFrekuensi writeFrekuensi = new WriteFrekuensi();
        WriteMean writeMean = new WriteMean();
        menuTop();
        System.out.println("\nLetakkan file csv dengan nama file data_sekolah.csv di direktori\nberikut: C:/temp/direktori\n");
        System.out.println("pilih menu:");
        System.out.println("1. Generate txt untuk menampilkan frekuensi nilai");
        System.out.println("2. Generate txt untuk menampilkan nilai rata-rata, median dan modus");
        System.out.println("3. Generate kedua file");
        System.out.println("0. exit");
        System.out.print("\npilihan anda : ");
        String pilihMenu = sc.next();
        System.out.println();
        AbstractWrite.pilihan = pilihMenu;
        switch (pilihMenu) {
            case "0" :
                System.out.println("Aplikasi ditutup");
                System.exit(0);
            case "1" :
                writeFrekuensi.tulis();
                back();
                break;
            case "2" :
                writeMean.tulis();
                break;
            case "3" :
                writeFrekuensi.tulis();
                writeMean.tulis();
                break;
            default :
                inputMenuSalah();
        }
    }

    void back(){
        System.out.println("0. Exit");
        System.out.println("1. Kembali ke Menu utama\n");
        System.out.print("pilihan anda: ");
        String input = sc.next();
        switch (input){
            case "0" :
                System.out.println("Aplikasi ditutup");
                System.exit(0);
            case "1" :
                menuAwal();
            default:
                inputMenuSalah();
        }
    }

    void inputMenuSalah(){
        menuTop();
        System.out.println("\nInput yang anda masukkan salah\n");
        back();
    }

    void failedGenerate(){
        System.out.println();
        System.out.println("\nAplikasi gagal generate file.\n");
    }
}
