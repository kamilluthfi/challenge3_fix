import java.io.*;
import java.util.List;

public class ReadAndConvertToList {

    void convertToList(List<Integer> list) throws IOException {

        File file = new File("C:\\temp\\direktori\\data_sekolah.csv");
        if (!file.exists()) {
            throw new FileNotFoundException ("\nFile tidak ditemukan. Pastikan anda telah meletakkan file csv"+
                    "\ndengan nama file data_sekolah.csv di direktori berikut:"+
                    "\nC:/temp/direktori");
        }
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        String line = "";
        String[] tempArr;
        while ((line = br.readLine()) != null) {
            tempArr = line.split(";");
            for (int i = 1; i < tempArr.length; i++) {
                list.add(Integer.parseInt(tempArr[i]));
            }
        }
        br.close();
    }
}
