import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import static org.junit.jupiter.api.Assertions.*;

public class MedianTest {

    private final List<Integer> listNilai = new ArrayList<>();
    private final ReadAndConvertToList convert = new ReadAndConvertToList();

    @Test
    @DisplayName("Test cari median")
    void testMedian () throws IOException {

        convert.convertToList(listNilai);

        List <Integer> listSorted = listNilai.stream().sorted().collect(Collectors.toList());

        float hasil;
        int indexMedian = listSorted.size()/2;
        if (listSorted.size()%2==0){
            hasil = listSorted.get(((indexMedian + (indexMedian + 1)) / 2));
        } else {
            hasil = listSorted.get(indexMedian+1);
        }
        assertEquals("8.0",String.valueOf(hasil) );

    }

}
